# quickscript

Create quick command line scripts from templates.

# Example of use

create a quickscript from existing command

```
# we download version 1.2.3 from an address:
curl -o v_1.2.3.tgz http://example.com/version1/version_1.2.3.tar.gz

# now we want to download 0.4.5 and 2.1.1 as well. instead of editing the command we create a 'quickscript' called "dl" from a template:

qs --create dl curl -o v_1.2.3.tgz http://example.com/version1/version_1.2.3.tar.gz

# qs --create <name of quick script> <basis for template>

# this opens a text editor with everyting after "dl" to edit it into a template
# what we want is two templates: the version (1.2.3) and the directory the file is in (version1)
# the script uses `sh` shell syntax. 
# a script could look like this

curl -o v_$1.tgz http://example.com/version${2:-0}/version_$1.tar.gz

# we save the text document like this and have created the quickscript "dl"
# it has two positional arguments $1 and $2. If $2 is not present it will have the default value of "0".
# now we can invoke the quickscript like this:

# to download 0.4.5
qs dl 0.4.5

# this will result in the following script being executed
curl -o v_0.4.5.tgz http://example.com/version0/version_0.4.5.tar.gz

# to download 2.1.1
qs dl 2.1.1 2

# this will result in the following script being executed
curl -o v_2.1.1.tgz http://example.com/version2/version_2.1.1.tar.gz
```

## Named arguments

It is also possible to use named arguments. Given a quickscript like the following:

```
curl -o v_${version}.tgz http://example.com/version${short:-0}/version_${version}.tar.gz
```

You can invoke it like this to download verison 2.1.1:

```
qs dl version=2.1.1 short=2
```
