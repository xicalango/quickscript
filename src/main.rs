
use serde::{
    Serialize,
    Deserialize,
};

use clap::clap_app;

use std::collections::HashMap;

use std::convert::From;
use std::process::{
    Command,
    ExitStatus,
};

use std::path::{
    Path,
    PathBuf,
};

use std::fs::{
    self,
    OpenOptions,
    File,
};

use std::os::unix::fs::OpenOptionsExt;

use std::io::{
    Read,
    Write,
    SeekFrom,
    Seek,
};

use std::env;

use tempfile::NamedTempFile;

use clap::ArgMatches;

type Result<T> = std::result::Result<T, failure::Error>;

// TODO extract structs in files

#[derive(Debug, Deserialize, Serialize)]
struct Script {
    definition: String,
}

impl<'a> From<&'a str> for Script {
    fn from(def: &'a str) -> Script {
        Script::new(def.to_string())
    }
}

impl Script {

    pub fn new(definition: String) -> Script {
        Script {
            definition,
        }
    }
    
    pub fn from_stdin() -> Result<Script> {
        let stdin = std::io::stdin();
        let handle = stdin.lock();

        Script::from_read(handle)
    }

    pub fn from_file<P: AsRef<Path>>(p: P) -> Result<Script> {
        let file = File::open(p)?;
        Script::from_read(file)
    }

    pub fn from_read<R: Read>(mut read: R) -> Result<Script> {
        let mut buffer = String::new();

        read.read_to_string(&mut buffer)?;

        Ok(Script::new(buffer))
    }

    pub fn invoke(&self, cmd: &str, args: &Args) -> Result<ExitStatus> {
        Command::new("sh")
            .arg("-c")
            .arg(&self.definition)
            .arg(cmd)
            .args(&args.pos_args)
            .envs(&args.kw_args)
            .status()
            .map_err(failure::err_msg)
    }

    pub fn definition(&self) -> &String {
        &self.definition
    }

    pub fn export_as_shell_script<W: Write>(&self, mut output: W) -> Result<()> {
        writeln!(output, "#!/bin/sh")?;
        write!(output, "{}", &self.definition)?;
        Ok(())
    }

    pub fn export_to_file<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        let file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .mode(0o755)
            .open(path)?;

        self.export_as_shell_script(file)
    }
}

#[derive(Debug)]
struct Args {
    pub pos_args: Vec<String>,
    pub kw_args: HashMap<String, String>,
}

impl Args {

    pub fn from_strs(args: &Vec<&str>) -> Result<Args> {
        let mut pos_args: Vec<String> = Vec::new();
        let mut kw_args: HashMap<String, String> = HashMap::new();

        for &arg in args {
            if arg.contains('=') {
                let kvs: Vec<&str> = arg.splitn(2, "=").collect();

                if kvs[0].len() == 0 {
                    return Err(failure::err_msg(format!("Missing key in {}", arg)));
                }

                if kw_args.contains_key(kvs[0]) {
                    return Err(failure::err_msg(format!("Duplicate key: {}", kvs[0])));
                }

                kw_args.insert( kvs[0].to_string(), kvs[1].to_string());
            } else {
                pos_args.push(arg.to_string());
            }
        }

        Ok(Args { pos_args, kw_args })
    }

}

#[derive(Debug, Serialize)]
struct ScriptDefintions {
    scripts: HashMap<String, Script>,
}

#[derive(Debug, Deserialize)]
struct LoadingScriptDefintions {
    scripts: Option<HashMap<String, Script>>,
}

impl From<LoadingScriptDefintions> for ScriptDefintions {
    fn from(defs: LoadingScriptDefintions) -> ScriptDefintions {
        let scripts = defs.scripts.unwrap_or_else(HashMap::new);
        ScriptDefintions { scripts }
    }
}

impl ScriptDefintions {
    pub fn empty() -> ScriptDefintions {
        ScriptDefintions {
            scripts: HashMap::new(),
        }
    }

    pub fn get(&self, key: &str) -> Option<&Script> {
        self.scripts.get(key)
    }

    pub fn get_mut(&mut self, key: &str) -> Option<&mut Script> {
        self.scripts.get_mut(key)
    }

    pub fn insert(&mut self, key: &str, script: Script) {
        self.scripts.insert(key.to_string(), script);
    }

    pub fn remove(&mut self, key: &str) {
        self.scripts.remove(key);
    }
    
    pub fn write_to_path<P: AsRef<Path>>(&self, p: P) -> Result<()> {
        let mut file = OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(p)?;

        let exported = toml::to_string(&self).map_err(failure::err_msg)?;
        
        write!(file, "{}", exported)?;

        Ok(())
    }

    pub fn from_path<P: AsRef<Path>>(p: P) -> Result<ScriptDefintions> {
        if !p.as_ref().exists() {
            return Ok(ScriptDefintions::empty());
        }

        let mut file = OpenOptions::new()
            .read(true)
            .open(p)?;

        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let loaded_defs: Result<LoadingScriptDefintions> = toml::from_str(&contents).map_err(failure::err_msg);
        loaded_defs.map(ScriptDefintions::from)
    }

    pub fn iter(&self) -> impl Iterator<Item=(&String, &Script)> {
        self.scripts.iter()
    }

    pub fn list_to_stdout(&self, show_script: bool) {
        for (key, value) in self.scripts.iter() {
            if show_script {
                println!("{}={}", key, value.definition());
            } else {
                println!("{}", key);
            }
        }
    }
}

fn get_home_config_path() -> PathBuf {
    dirs::home_dir().map(|mut home| {home.push(".qsconfig"); home}).unwrap()
}

fn get_config_path(arg_path: Option<&str>, force_global: bool) -> impl AsRef<Path> {

    if force_global {
        assert!(arg_path.is_none());
        get_home_config_path()
    } else if let Some(arg_path) = arg_path {
        PathBuf::from(arg_path)
    } else {
        let scripts_file_path = PathBuf::from("Scriptsfile");
        if scripts_file_path.is_file() {
            scripts_file_path
        } else {
            get_home_config_path()
        }
    }
}

fn get_editor() -> String {
   env::var("EDITOR").ok().unwrap_or("vi".to_string())
}

fn edit_script_text(script: &str) -> Result<Script> {
    let editor = get_editor();

    let mut file = NamedTempFile::new()?;

    writeln!(file, "{}", script)?;

    let status = Command::new(&editor)
        .arg(file.path())
        .status()?;

    file.seek(SeekFrom::Start(0))?;

    if status.success() {
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let trimmed = contents.trim();

        Ok(trimmed.into())
    } else {
        let error_message = format!("editor failed{}", 
                                    status.code()
                                        .map(|s| format!("with status {}", s))
                                        .unwrap_or_else(String::new)
                                    );
        Err(failure::err_msg(error_message))
    }

}

fn create_script<P: AsRef<Path>>(cmd: &str, args: &Vec<&str>, scripts: &mut ScriptDefintions, p: P) -> Result<()> {
    if let Some(_) = scripts.get(cmd) {
        return Err(failure::err_msg(format!("Script already exists: {}", cmd)));
    }

    let script_text = args.join(" ");
    let script = edit_script_text(&script_text)?;
    scripts.insert(cmd, script);
    scripts.write_to_path(p)?;
    Ok(())
}

fn edit_script<P: AsRef<Path>>(cmd: &str, scripts: &mut ScriptDefintions, p: P) -> Result<()> {
    {
        let script = scripts.get_mut(cmd).ok_or_else(|| failure::err_msg(format!("Script not found: {}", cmd)))?;
        *script = edit_script_text(script.definition())?;
    }

    scripts.write_to_path(p)?;
    Ok(())
}

fn remove_script<P: AsRef<Path>>(cmd: &str, scripts: &mut ScriptDefintions, p: P) -> Result<()> {
    if scripts.get(cmd).is_none() {
        return Err(failure::err_msg(format!("Key not found: {}", cmd)));
    }
    scripts.remove(cmd);
    scripts.write_to_path(p)?;
    Ok(())
}

fn export_script<P: AsRef<Path>>(cmd: &str, scripts: &ScriptDefintions, to_path: Option<P>) -> Result<()> {
    let script = scripts.get(cmd).ok_or(failure::err_msg(format!("Script not found: {}", cmd)))?;

    if let Some(path) = to_path {
        script.export_to_file(path)
    } else {
        script.export_as_shell_script(std::io::stdout())
    }

}

fn dump_scripts<P: AsRef<Path>>(dir: P, scripts: &ScriptDefintions) -> Result<()> {
    let dir_path = dir.as_ref();
    if dir_path.exists() {
        return Err(failure::err_msg(format!("Path already exists: {:?}", dir_path)));
    }

    fs::create_dir(dir_path)?;

    for (script_key, script) in scripts.iter() {
        let mut buf = PathBuf::new();
        buf.push(dir_path);
        buf.push(script_key);
        buf.set_extension("sh");

        script.export_to_file(&buf)?;

        println!("Exported {} as {:?}", script_key, buf);
    }
    
    Ok(())
}

fn run(matches: ArgMatches) -> Result<()> {
    // TODO clean up this mess of a function

    let args: Vec<&str> = matches.values_of("args").map(|p| p.collect()).unwrap_or_else(Vec::new);

    let config_path = get_config_path(matches.value_of("CONFIG"), matches.is_present("GLOBAL"));
    let mut scripts = ScriptDefintions::from_path(&config_path)?;

    if matches.is_present("action") {

        if matches.is_present("LIST") {
            Ok(scripts.list_to_stdout(false))
        } else if matches.is_present("LONG_LIST") {
            Ok(scripts.list_to_stdout(true))
        } else if let Some(cmd) = matches.value_of("CREATE") {
            create_script(&cmd, &args, &mut scripts, &config_path)
        } else if let Some(cmd) = matches.value_of("EDIT") {
            edit_script(&cmd, &mut scripts, &config_path)
        } else if let Some(cmd) = matches.value_of("REMOVE") {
            remove_script(&cmd, &mut scripts, &config_path)
        } else if let Some(cmd) = matches.value_of("EXPORT") {
            export_script(&cmd, &scripts, matches.value_of("OUTPUT"))
        } else if let Some(dir) = matches.value_of("DUMP") {
            dump_scripts(&dir, &scripts)
        } else {
            panic!()
        }

    } else {
        
        if matches.is_present("STDIN") {
            let script = Script::from_stdin()?;
            let args = Args::from_strs(&args)?;
            script.invoke("<stdin>", &args).map(|_| ())
        } else if matches.is_present("FILE") {
            let file_path = matches.value_of("FILE").unwrap();
            let file_name = Path::new(&file_path).file_name()
                .and_then(|s| s.to_str())
                .unwrap_or("");

            let script = Script::from_file(&file_path)?;
            let args = Args::from_strs(&args)?;
            script.invoke(&file_name, &args).map(|_| ())
        } else {
            let splitted_args = args.split_first().ok_or(failure::err_msg(matches.usage().to_string()))?;
            let cmd = &splitted_args.0;
            let cmd_args = splitted_args.1.to_vec();

            if let Some(script) = scripts.get(cmd) {
                let args = Args::from_strs(&cmd_args)?;
                script.invoke(cmd, &args).map(|_| ())
            } else {
                Err(failure::err_msg(format!("script not found: {}", cmd)))
            }
        }
    }
}

fn main() {
    
    let matches = clap_app!(quickscript =>
        (author: "Alexander Weld")
        (version: "1.0")
        (@group config_source =>
            (@arg GLOBAL: --global "Read quickscripts from ~/.qsconfig only")
            (@arg CONFIG: --config [FILE] "Read quickscript registry from this file. Defaults to ~/.qsconfig")
        )
        (@arg OUTPUT: -o --output [FILE] "(Only valid when exporting) Export to this file instead of stdout")
        (@group action =>
            (@arg CREATE: -c --create [CMD] "Create quickscript from command")
            (@arg EDIT: -e --edit [CMD] "Edit existing quickscript")
            (@arg REMOVE: -r --rm [CMD] "Remove quickscript from registry")
            (@arg EXPORT: -x --export [CMD] "Export quickscript to stdout. Output file can be set with -o")
            (@arg DUMP: -d --dump [DIR] "Export all quickscripts into the directory DIR")
            (@arg LIST: -l --list "List all registered quickscripts")
            (@arg LONG_LIST: --("long-list") "List all registered quickscripts with definitions")
        )
        (@group source =>
            (@arg STDIN: -s --stdin "Read quickscript from stdin instead of registry")
            (@arg FILE: -f --file [FILE] "Read quickscript from given file instead of registry")
        )
        (@arg args: ...)
    ).setting(clap::AppSettings::TrailingVarArg)
        .get_matches();

    let result = run(matches);

    if let Err(e) = result {
        println!("{}", e);
    }
}


#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_args_from_vec() {
        let strs = vec!["test", "bla=blubb", "gna", "tsa=tsupp", "foo=gna gna gna"];
        
        let args = Args::from_strs(&strs).unwrap();

        assert_eq!(args.get("0"), Some("test"));
        assert_eq!(args.get("1"), Some("gna"));
        assert_eq!(args.get("2"), None);
        assert_eq!(args.get("bla"), Some("blubb"));
        assert_eq!(args.get("tsa"), Some("tsupp"));
        assert_eq!(args.get("foo"), Some("gna gna gna"));
    }

    #[test]
    #[should_panic]
    fn test_args_invalid() {
        Args::from_strs(&vec!["=a"]).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_args_duplicate_key() {
        Args::from_strs(&vec!["a=a", "a=a"]).unwrap();
    }

    #[test]
    fn test_invoke() {
        let pos_args = vec!["test".to_string()];
        let mut kw_args = HashMap::new();
        kw_args.insert("dir".to_string(), "files".to_string());
        
        let args = Args {
            pos_args,
            kw_args,
        };

        let script: Script = "echo $0\necho $1 ${dir}".into();

        script.invoke( "test", &args ).unwrap();

    }

    #[test]
    fn test_serialize() {
        let mut scripts = HashMap::new();

        scripts.insert("test".to_string(), "a{0}, b{1}".into());
        scripts.insert("dl".to_string(), "a{test}, b{best}".into());
        scripts.insert("multiline".to_string(), "a{test}\nb{best}".into());

        let defs = ScriptDefintions {
            scripts,
        };

        let toml = toml::to_string(&defs).unwrap();

        println!("{}", toml);
    }

    #[test]
    fn empty_args() {
        let empty_args = Args::empty();
        assert_eq!(empty_args.pos_args.len(), 0);
        assert_eq!(empty_args.kw_args.len(), 0);
    }
}
